#!/bin/bash

## Initial base variable
webRootPath="/home/kovalyov/www_projects/localhost"
backupPath="/mnt/S/Kovalyov/backup"
dbName="root"
dbPass="dP9uCX"

## Get project info
echo " \033[91m*** Create magento backup ***\033[0m "
echo "Enter project name and branch (space separated):"
read project branch
echo "Enter DB name:"
read db_name
echo ""

if [ ! -d "$webRootPath/$project/$branch" ]; then
    echo " !! Project doesn't exist !!"
    exit 1
fi

## Create directory base directory and replace exsisting
if [ ! -d "$backupPath" ]; then
    mkdir ${backupPath}
fi
if [ ! -d "$backupPath/$project" ]; then
    mkdir ${backupPath}/${project}
else
    ## Delete old branch backups
    rm ${backupPath}/${project}/${db_name}.sql.tar.gz
    rm ${backupPath}/${project}/${branch}.tar.gz
fi

## Create database and code backup
echo "Create code backup ..."
cd ${webRootPath}/${project} && tar czf ${backupPath}/${project}/${branch}.tar.gz ${branch} --exclude='var/cache' --exclude='var/session' --exclude='app/etc/local.xml'

echo "Create database backup (need ROOT password) ..."
mysqldump -u${dbName} -p${dbPass} ${db_name} > ${backupPath}/${project}/${db_name}.sql
cd ${backupPath}/${project} && tar czf ${db_name}.sql.tar.gz ${db_name}.sql
rm ${backupPath}/${project}/${db_name}.sql

## Show final message
echo "Complete backup in \033[91m${backupPath}/${project}/\033[0m"
ls -lh ${backupPath}/${project}/
echo " \033[91m*** Finish create magento backup ***\033[0m "



